<?php include ("_includes/Gheader.php")?>

<div id="wowslider-container1">  <!-- BEGIN TOP SLIDER DIV -->

		<div class="ws_images"><ul> <!-- BEGIN WS_IMAGES DIV -->
		
		<li><a href="http://www.piccexcellence.com/about-picc-excellence/staff/nancy-bio/"><img src="/data1/images/announcements1.jpg" alt="announcements" title="announcements" id="wows1_11"/></a></li>
	
					
		<li><a href="http://www.vesselhealth.org/archived.html" target="_blank"><img src="/data1/images/vesselHealth.png" alt="vessel health archive" title="vessel health archive" id="wows1_7"/></a></li>
		
		<li><a href="http://www.piccexcellence.com/?p=5027" target="_blank"><img src="/data1/images/midline_new.jpg" alt="midline" title="midine" id="wows1_1"/></a></li> 
		
						
						
		<li><a href="http://www.piccexcellence.com/picciv-education/online-insertion-training/online-piv-ultrasound-guided-peripheral-insertion/"><img src="/data1/images/piv_new.jpg" alt="piv" title="piv" id="wows1_10"/></a></li>
	
		
		<li><a href="http://www.piccexcellence.com/picciv-education/online-insertion-training/online-basic-picc-insertion"><img src="/data1/images/basic_new.jpg" alt="basic PICC" title="basic PICC" id="wows1_3"/></a></li>
		
		<li><a href="http://www.piccexcellence.com/online-insertion-training/online-ultrasound-picc-insertion"><img src="/data1/images/ultrasound_new.jpg" alt="ultrasound" title="ultrasound" id="wows1_5"/></a></li>
		
		<li><a href="http://www.piccexcellence.com/picciv-education/online-insertion-training/online-ekgterminal-tip-positioning-systems-for-cvcs/"><img src="/data1/images/ekg_new.jpg" alt="ekg" title="ekg" id="wows1_6"/></a></li>
		
		<li><a href="http://www.piccexcellence.com/consulting"><img src="/data1/images/consultants_new.jpg" alt="consulting_header" title="consulting_header" id="wows1_2"/></a></li>
		
		<li><a href="http://www.piccexcellence.com/cpui-main"><img src="/data1/images/cpui_new.jpg" alt="cpui_header" title="cpui_header" id="wows1_4"/></a></li>
			
								
	     <li><a href="http://www.piccexcellence.com/site-license/benefits-site-license"><img src="/data1/images/site_licensing_new.jpg" alt="site_license_header" title="site_license_header" id="wows1_8"/></a></li>
			
		<li><a href="http://www.piccexcellence.com/workshops"><img src="/data1/images/workshop_new.jpg" alt="workshops_header" title="workshops_header" id="wows1_9"/></a></li>
			
	</ul>
	</div> <!-- END WS_IMAGES DIV -->
	
	<div class="ws_bullets"><div>
			
</div>  <!-- END WOWSLIDER-CONTAINER1 DIV -->


<div class="container">
		
        <div class="welcome"> Welcome to <br>
					PICC Excellence Inc.
        </div>
        
        <div class="welcome-sub"> Quality education meets affordablity</div>
        
        <div class="welcome-promo-vid">
        <img src="images/promo-vid-still.png">
        </div>
        
        <div class="welcome-text-head">What we do for you.</div>
        
        <div class="welcome-text">We’re a company that prides itself on creating stand out good quality education services for clinicians worldwide. Whether you’re just starting with vascular access or you’re a veteran looking to become an instructor we have a course to service your needs.<br><br>
How do we continually provide excellence in vascular access education to such a wide variety of clinicians? Nancy and our team of professionals are constantly researching the field of vascular access and education to bring you the most current, effective practices available.</div>

		<!--<div class="describe-you">What best describes you?</div>
        <div class="describe-you-sub">Make use of our education fast tracks, designed to service your individual needs.</div>-->
        
        <div class="fast-track-button" style="margin-right:17px;">I'm brand new to Vascular Access</div>
        <div class="fast-track-button" style="margin-right:17px;">I'm looking for <span style="font-size:22px;">Ultrasound Training</span></div>
        <div class="fast-track-button">I need to update my knowledge</div>
        
        <div class="fast-track-big-guy"> I want full access, please. </div>
        
        <div class="simple-link"> Click here for the Master Course Catalogue</div>

</div>

<div class="sidebar" style="padding-bottom:100px;">
<?php include ("_includes/footer.php")?>
