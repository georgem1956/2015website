<!DOCTYPE html>
<?php

///TURN ON OUTPUT BUFFERING SO PHP HEADER WILL WORK CORRECTLY///

ob_start();

?>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>
	
		
	
	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
		
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	
	
	<!--[if IE]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
		<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'></script>
		<script src='js/example.js'></script>
		
		
	<!-- TOP SLIDER -->
	<link rel="stylesheet" type="text/css" href="/engine1/style.css" />
	
	<script type="text/javascript" src="/engine1/jquery.js">	
	
	
	
	</script>
	
	
	<!-- BOTTOM SLIDER   -->
	<link rel="stylesheet" type="text/css" href="/engine2/style.css" />
	<script type="text/javascript" src="/engine2/jquery.js"></script>
	
	
	
	
	<script type="text/javascript">
	<!--
	
	function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}
	
	function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	  if(!x && d.getElementById) x=d.getElementById(n); return x;
	}
	
	function MM_swapImage() { //v3.0
	  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	
	function MM_reloadPage(init) {  //reloads the window if Nav4 resized
	  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
	  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
	}
	MM_reloadPage(true);
	function MM_showHideLayers() { //v9.0
	  var i,p,v,obj,args=MM_showHideLayers.arguments;
	  for (i=0; i<(args.length-2); i+=3) 
	  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
	    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
	    obj.visibility=v; }
	}
	
	
    	  
	//-->
	</script>
	
	
	<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
	
	
	
		
	
	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
	
	
    
</head>

<body <?php body_class(); ?>>
	
	<div id="page-wrap" class="group">

		<div id="header">
		
			<div id="top-line"></div>
			
			<div id="logo">		</div> 
		<!--	<a href="http://www.piccexcellence.com"><img src="/images/logo1.png" alt="logo" title="logo"/></a>-->
		
			<nav id="navbar">
			
			</nav> 
			
	
			
		<?php do_action('wp_menubar',''); ?> 
			
		</div> <!-- end header div -->
		
		
		
	<div id="banner">
	
	
			<div id="wowslider-container1">
				<div class="ws_images"><ul>
				
				<li><a href="http://www.piccexcellence.com/about-picc-excellence/staff/nancy-bio/"><img src="/data1/images/announcements1.jpg" alt="announcements" title="announcements" id="wows1_11"/></a></li>
			
							
				<li><a href="http://www.vesselhealth.org/archived.html" target="_blank"><img src="/data1/images/vesselHealth.png" alt="vessel health archive" title="vessel health archive" id="wows1_7"/></a></li>
				
				<li><a href="http://www.piccexcellence.com/?p=5027" target="_blank"><img src="/data1/images/midline_new.jpg" alt="midline" title="midine" id="wows1_1"/></a></li> 
				
								
								
				<li><a href="http://www.piccexcellence.com/picciv-education/online-insertion-training/online-piv-ultrasound-guided-peripheral-insertion/"><img src="/data1/images/piv_new.jpg" alt="piv" title="piv" id="wows1_10"/></a></li>
			
				
				<li><a href="http://www.piccexcellence.com/picciv-education/online-insertion-training/online-basic-picc-insertion"><img src="/data1/images/basic_new.jpg" alt="basic PICC" title="basic PICC" id="wows1_3"/></a></li>
				
				<li><a href="http://www.piccexcellence.com/online-insertion-training/online-ultrasound-picc-insertion"><img src="/data1/images/ultrasound_new.jpg" alt="ultrasound" title="ultrasound" id="wows1_5"/></a></li>
				
				<li><a href="http://www.piccexcellence.com/picciv-education/online-insertion-training/online-ekgterminal-tip-positioning-systems-for-cvcs/"><img src="/data1/images/ekg_new.jpg" alt="ekg" title="ekg" id="wows1_6"/></a></li>
				
				<li><a href="http://www.piccexcellence.com/consulting"><img src="/data1/images/consultants_new.jpg" alt="consulting_header" title="consulting_header" id="wows1_2"/></a></li>
				
				<li><a href="http://www.piccexcellence.com/cpui-main"><img src="/data1/images/cpui_new.jpg" alt="cpui_header" title="cpui_header" id="wows1_4"/></a></li>
					
										
			     <li><a href="http://www.piccexcellence.com/site-license/benefits-site-license"><img src="/data1/images/site_licensing_new.jpg" alt="site_license_header" title="site_license_header" id="wows1_8"/></a></li>
					
				<li><a href="http://www.piccexcellence.com/workshops"><img src="/data1/images/workshop_new.jpg" alt="workshops_header" title="workshops_header" id="wows1_9"/></a></li>
					
			</ul></div>
			<div class="ws_bullets"><div>
					
		</div>
	</div>
	<!-- Generated by WOWSlider.com v2.7.1 -->
		<div class="ws_shadow"></div>
		</div>
		<script type="text/javascript" src="/engine1/wowslider.js"></script>
		<script type="text/javascript" src="/engine1/script.js"></script>
		<!-- End WOWSlider.com BODY section -->
	

	</div>
	
	
	
	
	