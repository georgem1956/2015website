<!-- REINSERTED DIV - TOPSLIDER - FOR THE TOP SLIDER TO POSITION IT IN THE CORRECT LOCATION ON THE PAGE - GDM - 06/24/2015-->

<?php include ("_includes/header.php")?>


<div id="topSlider"> <!-- BEGIN TOP SLIDER DIV -->

<?php include("_includes/news-slideshow.php");?>	

</div> <!-- END TOP SLIDER DIV -->

<div class="content">
		
        <div class="welcome"> Welcome to <br>
					PICC Excellence Inc.
        </div>
        
        <div class="welcome-sub"> Quality education meets affordablity</div>
        
        <div class="welcome-promo-vid">
        <img src="images/promo-vid-still.png">
        </div>
        
        <div class="welcome-text-head">What we do for you.</div>
        
        <div class="welcome-text">We’re a company that prides itself on creating stand out good quality education services for clinicians worldwide. Whether you’re just starting with vascular access or you’re a veteran looking to become an instructor we have a course to service your needs.<br><br>
How do we continually provide excellence in vascular access education to such a wide variety of clinicians? Nancy and our team of professionals are constantly researching the field of vascular access and education to bring you the most current, effective practices available.</div>

		<!--<div class="describe-you">What best describes you?</div>
        <div class="describe-you-sub">Make use of our education fast tracks, designed to service your individual needs.</div>-->
        
       
        <div class="blue-button" style="margin-right:17px;">I'm brand new to Vascular Access</div>
        <div class="blue-button" style="margin-right:17px;">I'm looking for <span style="font-size:22px;">Ultrasound Training</span></div>
        <div class="blue-button">I need to update my knowledge</div>
        
        <div class="big-blue-button"> I want full access, please. </div>
        
        <div class="simple-link"> Click here for the Master Course Catalogue</div>

</div> <!-- END DIV CONTENT -->

<div class="sidebar" style="padding-bottom:42px;">
<?php include ("_includes/footer.php")?>
