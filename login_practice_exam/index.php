<?php 
	include("../_includes/path.php");
	include("../_includes/dbconn.php");
	
	$startdate = $_GET["startd"];
	$stopdate = $_GET["stopd"];
	

	/// LOGIN ERROR RESPONSES
	$lrspx = 2; 
	$lrespj = "";
	$lrespj2 = 0;
	$lrspmgx = "";
	if(isset($_GET["lresponse"]))
	{
		$lrespj = $_GET["lresponse"];
		$lrespj2 = intval($_GET["lresponse"]);

		if($lrespj == $lrespj2)
		{
			$lrspmg[0] = "";
			$lrspmg[1] = "You did not include some required information. Please review your information and try again.";
			$lrspmg[2] = "The login information that you entered does not appear to be valid. Please try again.";
			$lrspmg[3] = "Test start date has not been activated. Please contact the CPUI Administrator for further instructions." ;
			$lrspmg[4] = "Testing period has expired. Please contact the CPUI Administrator for further assistance." ;
			$lrspmg[5] =  "Today is not between start and stop dates of " . date("m/d/y", $startdate) . " - " . date("m/d/y", $stopdate) . ". Please Contact the CPUI Administrator for further assistance.";
			
			$lrspmgx = $lrspmg[$lrespj];

			if($lrspmgx != "" && $lrspmgx != NULL)
			{
				$lrspx = 0;
			}
		}		
	}



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<link href="style.css" rel="stylesheet" type="text/css" media="screen">

<header>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>PICC Excellence, Inc.</title>




<script type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>

<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<style type="text/css">
<!--
.style2 {color: #FF0000}
-->
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body >

<div id="main_wrapper">

    <header> </header>




    <div id="pg_title"><span class="title">PICC Excellence <strong><font color="red">PRACTICE</font></strong> CPUI EXAM Login</span></div>
    
    
    
  <div id="lg_form"> <form name="form1" method="post" action="logina_cert.php">
            
      <p>	<strong>PICC Excellence Practice Exam Login:</strong></p>
                            
    
                        
                <p align="left"><span class="instrctns"><font size="4" color="red">(Use your PICC Excellence CPUI Course ID and password) </font><br />
</span><br />
                  <span> Email </span> <br />
                  <span>  
                  <input type="text" name="Email" size="20" maxlength="75" value="">
                  </span><br />
                  <span> Password </span><br />
                  <span>  
                  <input type="password" name="Password" size="20" maxlength="50" value="">
                  </span><br />
                  <input type="hidden" name="LoginPage" value="index.php" >
                  <span> 
                  <input type="submit" name="submit" value="Login">
                  </span>                </p>
                </form>
            
           <?php
                if($lrspmgx != "" && $lrspmgx != NULL)
                {
                    print("<p><font color=\"red\">" . $lrspmgx . "</font></p>");
                }
            ?>		
  </div>
</div> <!-- end main wrapper div -->      

</body>
</html>
